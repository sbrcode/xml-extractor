import argparse, os
import xml.etree.cElementTree as et

def parse_arguments():
    parser = argparse.ArgumentParser(description="Extract XML files from SMS Backup mobile app")
    parser.add_argument("-x","--xmlfile",help="""XML file containing smses""")
    return parser.parse_args()

if __name__ == "__main__":

    filename = parse_arguments().xmlfile
    parsedXML = et.parse(filename)
    filename_out = filename+'_sorted.txt'
    if os.path.exists(filename_out):
        os.remove(filename_out)

    for node in parsedXML.getroot():
        body = node.attrib.get('body')
        date = node.attrib.get('readable_date')
        who = node.attrib.get('type')
        cname = node.attrib.get('contact_name')
        if who == '1':
            who = str(cname[0]+cname[-1])
        else:
            who = 'Sn'
        new_xml = "{}\t{}:\t{}\n".format(date, who, body)

        with open(filename_out, 'a') as out:
            out.write(new_xml)